# MS3D Export Plugin for Blender 2.8

This is a brand new export plugin for Blender 2.8. It's not an import AND export, because situations for importing
are rare now. This is prodominantly written for the Simulant Game Engine which is an engine designed for turn-of-the-century
consoles.
