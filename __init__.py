

from .ms3d_main import register
from .ms3d_main import *  # noqa

bl_info = {
    "name": "MS3D Export Plugin",
    "blender": (2, 80, 0),
    "category": "Import-Export",
    "location": "File > Export > Milkshape3D (.ms3d)",
}

if __name__ == '__main__':
    register()
