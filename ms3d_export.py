import os
import shutil
from math import pi
from struct import pack

import bpy
import bpy.types
from bpy.props import BoolProperty
from bpy_extras import node_shader_utils
from bpy_extras.io_utils import (ExportHelper, axis_conversion,
                                 orientation_helper)

_SUPPORTED_TYPES = ('MESH',)


def _gather_objects(context, only_selected):
    assert(only_selected in (True, False))

    superset = (
        context.selected_objects
        if only_selected
        else context.scene.objects
    )

    return [x for x in superset if x.type == 'MESH']


def _triangulate_mesh(me):
    import bmesh
    bm = bmesh.new()
    bm.from_mesh(me)
    bmesh.ops.triangulate(bm, faces=bm.faces)
    bm.to_mesh(me)
    bm.free()


def _extract_animation_data(ob):
    """
        Returns a tuple of:
        (bones, pose_bones, action, nla_tracks)
    """
    try:
        armature_modifier = next(x for x in ob.modifiers if x.type == 'ARMATURE')
    except StopIteration:
        armature_modifier = None

    if armature_modifier:
        print(">> MS3D: Found an armature modifier (%s)" % armature_modifier)

    if ob.parent and ob.parent.type == 'ARMATURE' and ob.parent.pose:
        print(">> MS3D: Extracting armature from parent (%s)" % ob.parent)
        return (
            _sort_bones(ob.parent.data.bones),
            _sort_bones(ob.parent.pose.bones),
            getattr(ob.parent.animation_data, "action", None),
            getattr(ob.parent.animation_data, "nla_tracks", None),
            ob.parent.matrix_basis
        )
    elif armature_modifier:
        print(">> MS3D: Extracting armature from modifier (%s)" % armature_modifier)
        return (
            _sort_bones(armature_modifier.object.data.bones),
            _sort_bones(armature_modifier.object.pose.bones),
            armature_modifier.object.animation_data.action,
            armature_modifier.object.animation_data.nla_tracks,
            armature_modifier.object.matrix_basis
        )
    else:
        # No animation data found
        print(">> MS3D: No animation data found")
        return None


def _sort_bones(bone_collection):
    """ Make sure parent bones are before the children """
    result = []
    for bone in bone_collection:
        if bone.parent:
            try:
                # Insert after the parent bone if it's there, otherwise
                # insert at the start
                result.insert(result.index(bone.parent) + 1, bone)
                continue
            except ValueError:
                pass
        result.insert(0, bone)
    return result


def export_ms3d(context, config):
    print(">> MS3D: Exporting to %s" % config['output_file'])

    to_export = _gather_objects(context, config['only_selected'])
    depsgraph = context.evaluated_depsgraph_get()

    print(">> MS3D: About to export %s" % to_export)

    vertices = []
    triangles = []
    materials = []

    vertex_weights = []

    bones = []

    # We might be outputting multiple meshes into a single MS3D, so for
    # each chunk of mesh vertices we need to store the "offset" where their
    # vertex data begins in the final vertex list

    vertex_offset = 0
    material_offset = 0

    for i, obj in enumerate(to_export):
        if obj.parent and (obj.parent.instance_type in {'VERTS', 'FACES'}):
            continue

        obs = [(obj, obj.matrix_world)]
        if obj.is_instancer:
            obs += [
                (dup.instance_object.original, dup.matrix_world.copy())
                for dup in depsgraph.object_instances
                if dup.parent and dup.parent.original == obj
            ]

        for ob, ob_mat in obs:
            ob_for_convert = (
                ob.evaluated_get(depsgraph)
                if config['apply_modifiers']
                else ob.original
            )

            try:
                converted_mesh = ob_for_convert.to_mesh()
            except RuntimeError:
                continue

            converted_mesh.transform(config['transformation'] @ ob_mat)

            # Make sure we're working with triangles
            _triangulate_mesh(converted_mesh)

            # Store the vertex offsets
            vertex_offset = len(vertices)
            material_offset = len(materials)

            # Append the vertices to the end of the full vertex list
            vertices.extend(converted_mesh.vertices[:])
            materials.extend(converted_mesh.materials[:])

            for face in converted_mesh.polygons:
                uv0_idx = face.loop_indices[0]
                uv1_idx = face.loop_indices[1]
                uv2_idx = face.loop_indices[2]

                uv0 = converted_mesh.uv_layers.active.data[uv0_idx].uv
                uv1 = converted_mesh.uv_layers.active.data[uv1_idx].uv
                uv2 = converted_mesh.uv_layers.active.data[uv2_idx].uv

                # Texture coordinates are vertically flipped compared to MS3D
                uv0.y = 1.0 - uv0.y
                uv1.y = 1.0 - uv1.y
                uv2.y = 1.0 - uv2.y

                # Reverse winding on triangles for MS3D
                triangles.append(
                    (
                        face.material_index + material_offset,
                        face.vertices[0] + vertex_offset,
                        face.vertices[2] + vertex_offset,
                        face.vertices[1] + vertex_offset,
                        uv0, uv2, uv1
                    )
                )

            def matrix_difference(mat_src, mat_dst):
                mat_dst_inv = mat_dst.inverted()
                return mat_dst_inv @ mat_src

            def change_basis(value):
                return (-value[0], value[2], value[1])

            ninty_degrees = pi / 2

            animation_data = _extract_animation_data(ob)
            if animation_data:
                is_leaf = {}

                for bone in animation_data[0]:
                    if bone.name not in is_leaf:
                        is_leaf[bone.name] = True

                    if bone.parent:
                        is_leaf[bone.parent.name] = False

                for bone in animation_data[0]:
                    if bone.parent:
                        matrix = matrix_difference(
                            animation_data[-1] @ bone.matrix_local,
                            animation_data[-1] @ bone.parent.matrix_local
                        )
                    else:
                        matrix = animation_data[-1] @ bone.matrix_local

                    matrix = config['transformation'] @ matrix

                    bones.append({
                        'name': bone.name,
                        'parent_name': getattr(bone.parent, "name", ""),
                        'bone': bone,
                        'parent': bone.parent,
                        'rotation': matrix.to_euler(),
                        'translation': matrix.to_translation()
                    })

                    # FIXME: Investigate why we need to increase
                    # the X rotation by 90 degrees to get a correct output
                    bones[-1]['rotation'][0] += ninty_degrees

                    if is_leaf[bone.name]:
                        # Add a final joint to leaf nodes so that MS3D displays
                        # a physical bone
                        bones.append({
                            'name': "%s (leaf)" % bone.name,
                            'parent_name': bone.name,
                            'bone': None,
                            'parent': bone,
                            'rotation': (0, 0, 0),
                            'translation': bone.tail
                        })

            for vertex in converted_mesh.vertices[:]:
                # Calculate 4 bone weights for each vertex
                weights = []
                for grp in vertex.groups:
                    grp_name = ob.vertex_groups[grp.group].name
                    weights.append(
                        (grp.weight, grp_name)
                    )

                weights.sort()
                weights.reverse()

                if len(weights) > 4:
                    print(">> MS3D: Clipping weights. Had: %s" % weights)
                    # Retain 4 weights
                    # FIXME: If there are a load of weights, it seems possible that the
                    # recalculation of the 4th weight (from those clipped) might make
                    # it greater than the 3rd weight. May need better logic here.
                    weights = weights[:4]
                    weights[-1][0] = 1.0 - sum(x[0] for x in weights[:3])
                    print(">> MS3D: Clipped to: %s" % weights)

                while(len(weights) < 4):
                    weights.append((0.0, bones[0]["name"]))

                vertex_weights.append(weights)

    with open(config['output_file'], "wb") as f:
        fw = f.write

        # First, let's write the MS3D header
        fw(pack('cccccccccc', *[bytes(x, 'ascii') for x in "MS3D000000"]))
        fw(pack('I', 4))

        # Output the number of vertices
        fw(pack('H', len(vertices)))

        for i, vert in enumerate(vertices):
            # Write the vertex list out
            flags = 0
            bone_name = vertex_weights[i][0][1]  # First weight, but get the bone name

            try:
                # FIXME: Could be faster by precalculating indexes
                bone = next(i for i, v in enumerate(bones) if v["name"] == bone_name)
            except StopIteration:
                print("No bone for %s" % bone_name)
                bone = -1

            assert(len(vert.co) == 3)

            reference_count = 0
            fw(pack('b', flags))
            fw(pack('fff', *vert.co))
            fw(pack('b', bone))
            fw(pack('b', reference_count))

        # number of triangles
        fw(pack('H', len(triangles)))

        material_groups = {}

        for i, tri in enumerate(triangles):
            material_index, tri, uvs = tri[0], tri[1:4], tri[4:]
            material_groups.setdefault(material_index, []).append(i)

            flags = 0
            smoothing_group = 0
            group_index = 0

            assert(len(tri) == 3)

            fw(pack('H', flags))
            fw(pack('HHH', *tri))
            fw(pack('fff', *vertices[tri[0]].normal))
            fw(pack('fff', *vertices[tri[1]].normal))
            fw(pack('fff', *vertices[tri[2]].normal))
            fw(pack('fff', uvs[0].x, uvs[1].x, uvs[2].x))  # S
            fw(pack('fff', uvs[0].y, uvs[1].y, uvs[2].y))  # T
            fw(pack('b', smoothing_group))
            fw(pack('b', group_index))

        fw(pack('H', len(material_groups)))

        for k, v in material_groups.items():
            material = materials[k]

            fw(pack('B', 0))  # flags
            fw(pack('c' * 32, *[
                bytes(x, 'ascii') for x in material.name.ljust(32, "\0")
            ]))
            fw(pack('H', len(v)))

            for idx in v:
                fw(pack('H', idx))

            assert(k < 256)  # FIXME: MS3D only allows 256 materials
            fw(pack('B', k))

        fw(pack('H', len(materials)))

        def export_image(image):
            if not image.image:
                return ""

            source_dir = os.path.dirname(bpy.data.filepath)
            dest_dir = os.path.dirname(config['output_file'])
            source_file = os.path.join(source_dir, os.path.split(image.image.filepath)[-1])
            filename = os.path.split(source_file)[-1]
            dest_file = os.path.join(dest_dir, filename)

            print(">> MS3D Exporting image: %s => %s" % (source_file, dest_file))

            shutil.copyfile(source_file, dest_file)

            return filename

        def output_string(s, l):
            return [ord(x) for x in s.ljust(l, "\0")]

        for material in materials:
            mat_wrap = node_shader_utils.PrincipledBSDFWrapper(material)

            fw(pack('b' * 32, *output_string(material.name, 32)))
            fw(pack('ffff', 1, 1, 1, 1))  # FIXME: Does blender have an ambient equivalent?
            fw(pack('ffff', *mat_wrap.base_color, 1.0))
            fw(pack('ffff', *([mat_wrap.specular] * 4)))
            fw(pack('ffff', *mat_wrap.emission_color, 1.0))
            fw(pack('f', (1.0 * mat_wrap.roughness) * 128))
            fw(pack('f', mat_wrap.alpha))
            fw(pack('b', 0))  # Mode - unused
            fw(pack('b' * 128, *output_string(export_image(mat_wrap.base_color_texture), 128)))  # noqa
            fw(pack('b' * 128, *output_string(export_image(mat_wrap.alpha_texture), 128)))

        # Animation starts here!
        scene = context.scene
        total_frames = scene.frame_end - scene.frame_start
        fps = scene.render.fps
        current_frame = scene.frame_current

        fw(pack('f', fps))
        fw(pack('f', current_frame / fps))
        fw(pack('I', total_frames))

        print(">> MS3D FPS: %s Frames: %s  Current: %s" % (fps, total_frames, current_frame))

        fw(pack('H', len(bones)))

        # Write out joint data
        for bone in bones:
            fw(pack('b', 0))
            fw(pack('b' * 32, *output_string(bone["name"], 32)))
            fw(pack('b' * 32, *output_string(bone["parent_name"], 32)))
            fw(pack('fff', *bone["rotation"]))
            fw(pack('fff', *bone["translation"]))
            fw(pack('H', 0))
            fw(pack('H', 0))


@orientation_helper(axis_forward='-Z', axis_up='Y')
class MS3DExportOperator(bpy.types.Operator, ExportHelper):
    bl_idname = "export.ms3d"
    bl_label = "Export MS3D"

    filename_ext = ".ms3d"

    apply_modifiers = BoolProperty(
        name="Apply modifiers", default=True,
        description="Shall be modifiers applied during export?"
    )

    only_selected = BoolProperty(
        name="Only selected", default=True,
        description="What object will be exported? Only selected / all objects"
    )

    def execute(self, context):
        config = {
            'output_file': bpy.path.ensure_ext(self.filepath, ".ms3d"),
            'apply_modifiers': self.apply_modifiers,
            'only_selected': self.only_selected,
            'transformation': axis_conversion(
                to_forward=self.axis_forward,
                to_up=self.axis_up,
            ).to_4x4()
        }

        export_ms3d(context, config)

        return {'FINISHED'}
