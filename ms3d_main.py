import bpy

from .ms3d_export import MS3DExportOperator


def menu_func(self, context):
    self.layout.operator(
        MS3DExportOperator.bl_idname,
        text="Milkshape3D (.ms3d)"
    )


def register():
    bpy.types.TOPBAR_MT_file_export.append(menu_func)

    for klass in (MS3DExportOperator, ):
        bpy.utils.register_class(klass)


def unregister():
    for klass in (MS3DExportOperator, ):
        bpy.utils.unregister_class(klass)

    bpy.types.TOPBAR_MT_file_export.remove(menu_func)
